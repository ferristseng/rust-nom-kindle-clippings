use nom_kindle_clippings::parse_clippings;
use std::{fs::File, io::stdout};

fn main() {
    let mut clippings = Vec::new();
    let files = [
        "examples/clipping0.txt",
        "examples/clipping1.txt",
        "examples/clipping2.txt",
        "examples/clipping3.txt",
        "examples/clipping4.txt",
        "examples/clipping5.txt",
    ];

    for path in files.iter() {
        let raw = File::open(path).expect("expected to open example file");
        let file_clippings = parse_clippings(raw).expect("expected to parse input successfully");

        clippings.extend(file_clippings);
    }

    for clipping in clippings {
        println!("----------------------------");
        serde_json::to_writer_pretty(stdout(), &clipping).expect("expected to convert to json");
        println!("");
    }
}
