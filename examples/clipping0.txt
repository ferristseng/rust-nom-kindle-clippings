The Wretched of The Earth (Frantz Fanon)
- Highlight Loc. 925-26  | Added on Sunday, July 21, 2019, 10:00 PM

Challenging the colonial world is not a rational confrontation of viewpoints. It is not a discourse on the universal, but the impassioned claim by the colonized that their world is fundamentally different.
==========
