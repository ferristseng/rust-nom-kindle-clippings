use crate::{
    borrowed::{Bookmark, Clipping, Highlight, Note},
    error::ParseErrorWithInput,
    parser::{
        parse_clipping, parse_clipping_type, parse_created_at, parse_location_range,
        parse_page_number, parse_separator, parse_title_author_line, ClippingType,
    },
    LocationRange, Page,
};
use chrono::{TimeZone, Utc};
use nom::{error::ErrorKind, IResult};

#[test]
fn test_parse_separator_ok() {
    let expected: IResult<&[u8], (), _> = Ok((b"", ()));

    assert_eq!(parse_separator(b"==========\n"), expected);
}

#[test]
fn test_parse_separator_err() {
    let expected: IResult<&[u8], (), _> = Err(nom::Err::Error(ParseErrorWithInput(
        b"",
        ErrorKind::CrLf.into(),
    )));

    assert_eq!(parse_separator(b"=========="), expected);
}

#[test]
fn test_parse_title_no_author_ok() {
    let title = "Cracking the Coding Interview, 4 Edition - 150 Programming Interview Questions and Solutions";
    let expected: IResult<&[u8], _, _> = Ok((b"", (title, None)));
    let input = format!("{}\n", title);

    assert_eq!(parse_title_author_line(input.as_bytes()), expected);
}

#[test]
fn test_parse_title_with_author_ok() {
    let line = "The Wretched of The Earth (Frantz Fanon)";
    let expected: IResult<&[u8], _, _> =
        Ok((b"", ("The Wretched of The Earth", Some("Frantz Fanon"))));
    let input = format!("{}\n", line);

    assert_eq!(parse_title_author_line(input.as_bytes()), expected);
}

#[test]
fn test_parse_location_range_same_single_digit_range_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", LocationRange { start: 1, end: 2 }));

    assert_eq!(parse_location_range(b"Loc. 1-2 "), expected);
}

#[test]
fn test_parse_location_range_same_tens_digit_range_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", LocationRange { start: 24, end: 29 }));

    assert_eq!(parse_location_range(b"Loc. 24-9 "), expected);
}

#[test]
fn test_parse_location_range_same_hundreds_digit_range_ok() {
    let expected: IResult<&[u8], _, _> = Ok((
        b"",
        LocationRange {
            start: 686,
            end: 689,
        },
    ));

    assert_eq!(parse_location_range(b"Loc. 686-89 "), expected);
}

#[test]
fn test_parse_location_range_thousands_digit_range_ok() {
    let expected: IResult<&[u8], _, _> = Ok((
        b"",
        LocationRange {
            start: 1327,
            end: 1330,
        },
    ));

    assert_eq!(parse_location_range(b"Loc. 1327-30 "), expected);
}

#[test]
fn test_parse_created_at_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", Utc.timestamp(1563747660, 0)));

    assert_eq!(
        parse_created_at(b"Added on Sunday, July 21, 2019, 10:21 PM\n"),
        expected
    );
}

#[test]
fn test_parse_highlight_no_on_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", ClippingType::Highlight));

    assert_eq!(parse_clipping_type(b"- Highlight"), expected);
}

#[test]
fn test_parse_bookmark_no_on_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", ClippingType::Bookmark));

    assert_eq!(parse_clipping_type(b"- Bookmark"), expected);
}

#[test]
fn test_parse_highlight_with_on_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", ClippingType::Highlight));

    assert_eq!(parse_clipping_type(b"- Highlight on"), expected);
}

#[test]
fn test_parse_bookmark_with_on_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", ClippingType::Bookmark));

    assert_eq!(parse_clipping_type(b"- Bookmark on"), expected);
}

#[test]
fn test_page_ok() {
    let expected: IResult<&[u8], _, _> = Ok((b"", Page(3)));

    assert_eq!(parse_page_number(b"Page 3 "), expected);
}

#[test]
fn test_parse_clipping_highlight_ok() {
    let highlight = Clipping::Highlight(Highlight {
            title: "The Wretched of The Earth",
            author: Some("Frantz Fanon"),
            page: None,
            location: LocationRange { start: 925, end: 926 },
            created_at: Utc.timestamp(1563746400, 0),
            content: "Challenging the colonial world is not a rational confrontation of viewpoints. It is not a discourse on the universal, but the impassioned claim by the colonized that their world is fundamentally different."
        });
    let expected: IResult<&[u8], _, _> = Ok((b"", highlight));

    assert_eq!(
        parse_clipping(include_bytes!("../examples/clipping0.txt")),
        expected
    );
}

#[test]
fn test_parse_clipping_bookmark_ok() {
    let bookmark = Clipping::Bookmark(Bookmark {
            title: "Cracking the Coding Interview, 4 Edition - 150 Programming Interview Questions and Solutions",
            author: None,
            page: Some(Page(72)),
            location: None,
            created_at: Utc.timestamp(1395275580, 0),
        });
    let expected: IResult<&[u8], _, _> = Ok((b"", bookmark));

    assert_eq!(
        parse_clipping(include_bytes!("../examples/clipping1.txt")),
        expected
    )
}

#[test]
fn test_parse_clipping_highlight_with_page_ok() {
    let highlight = Clipping::Highlight(Highlight {
        title: "The Accidental Asian: Notes of a Native Speaker",
        author: Some("Eric Liu"),
        page: Some(Page(9)),
        location: LocationRange {
            start: 125,
            end: 125,
        },
        created_at: Utc.timestamp(1493817360, 0),
        content: "the mere act of observing a memory changes that memory’s meaning.",
    });
    let expected: IResult<&[u8], _, _> = Ok((b"", highlight));

    assert_eq!(
        parse_clipping(include_bytes!("../examples/clipping2.txt")),
        expected
    );
}

#[test]
fn test_parse_clipping_highlight_with_title_with_parens_ok() {
    let highlight = Clipping::Highlight(Highlight {
        title: "The poetry and brief life of a Foxconn worker: Xu Lizhi (1990-2014)",
        author: None,
        page: None,
        location: LocationRange {
            start: 103,
            end: 106,
        },
        created_at: Utc.timestamp(1586233080, 0),
        content: "仿似年轻打工者深埋于心底的爱情 Like the love that young workers bury at the bottom of their hearts 没有时间开口，情感徒留灰尘 With no time for expression, emotion crumbles into dust 他们有着铁打的胃 They have stomachs forged of iron 盛满浓稠的硫酸，硝酸 Full of thick acid, sulfuric and nitric 工业向他们收缴来不及流出的泪 Industry captures their tears before they have the chance to fall "
    });
    let expected: IResult<&[u8], _, _> = Ok((b"", highlight));

    assert_eq!(
        parse_clipping(include_bytes!("../examples/clipping5.txt")),
        expected
    );
}

#[test]
fn test_parse_clipping_bookmark_with_location_ok() {
    let bookmark = Clipping::Bookmark(Bookmark {
        title: "A Chinaman's Chance: One Family's Journey and the Chinese American Dream",
        author: Some("Eric Liu"),
        page: Some(Page(225)),
        location: Some(LocationRange {
            start: 3082,
            end: 3082,
        }),
        created_at: Utc.timestamp(1488173340, 0),
    });
    let expected: IResult<&[u8], _, _> = Ok((b"", bookmark));

    assert_eq!(
        parse_clipping(include_bytes!("../examples/clipping3.txt")),
        expected
    );
}

#[test]
fn test_parse_clipping_note_ok() {
    let note = Clipping::Note(Note {
        title: "How to Do Nothing",
        author: Some("Jenny Odell"),
        page: None,
        location: LocationRange {
            start: 2038,
            end: 2038,
        },
        created_at: Utc.timestamp(1584891600, 0),
        content: "how to do nothing = how to pay attention",
    });
    let expected: IResult<&[u8], _, _> = Ok((b"", note));

    assert_eq!(
        parse_clipping(include_bytes!("../examples/clipping4.txt")),
        expected
    );
}
