use crate::{
    borrowed::{Bookmark, Clipping, Highlight, Note},
    error::ParseErrorWithInput,
    LocationRange, Page,
};
use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
use nom::{
    branch::alt,
    bytes, character,
    combinator::{map, opt, recognize, value},
    sequence::{delimited, pair, preceded, separated_pair, terminated},
    IResult,
};
use std::str::{self, FromStr};

/// Used internally by the parser.
///
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ClippingType {
    Bookmark,
    Highlight,
    Note,
}

/// Parses an array of bytes as a UTF8-encoded string.
///
#[inline]
fn parse_string_utf8(s: &[u8]) -> Result<&str, nom::Err<ParseErrorWithInput>> {
    str::from_utf8(s).map_err(|e| nom::Err::Failure(ParseErrorWithInput(s, e.into())))
}

/// Parses out a number from a string.
///
fn parse_string_encoded_number(s: &[u8]) -> Result<u32, nom::Err<ParseErrorWithInput>> {
    let as_str = parse_string_utf8(s)?;
    let as_num =
        u32::from_str(as_str).map_err(|e| nom::Err::Failure(ParseErrorWithInput(s, e.into())))?;

    Ok(as_num)
}

/// Parses the separator inbetween Kindle clippings.
///
pub(crate) fn parse_separator(s: &[u8]) -> IResult<&[u8], (), ParseErrorWithInput> {
    map(
        pair(
            bytes::streaming::tag("=========="),
            character::complete::line_ending,
        ),
        |_| (),
    )(s)
}

static DIGIT_OR_SYMBOL: &[u8] = b"0123456789!@#$%^&*()-_=+,<.>/?;:'\"`~{[}]\\|";

fn parse_author(s: &[u8]) -> IResult<&[u8], &[u8], ParseErrorWithInput> {
    // '('       indicates start of author.
    // ')'       indicates end of author.
    delimited(
        bytes::streaming::tag("("),
        recognize(preceded(
            bytes::streaming::is_not(DIGIT_OR_SYMBOL),
            bytes::streaming::take_while(|c| c != b')'),
        )),
        bytes::streaming::tag(")"),
    )(s)
    .into()
}

/// Parses a line with the title and the author (if any).
///
pub(crate) fn parse_title_author_line(
    s: &[u8],
) -> IResult<&[u8], (&str, Option<&str>), ParseErrorWithInput> {
    // '('       indicates the start of an author section (if present).
    // '\n' '\r' indicates end of the line.
    let (rest, (title, author)) = terminated(
        alt((
            // Title with author.
            map(
                pair(bytes::streaming::is_not([b'\n', b'\r', b'(']), parse_author),
                |(title, author)| (title, Some(author)),
            ),
            // Title without author
            map(character::streaming::not_line_ending, |result| {
                (result, None)
            }),
        )),
        character::complete::line_ending,
    )(s)?;
    let title = parse_string_utf8(title)?;
    let title = title.trim_matches(|c: char| c.is_whitespace() || c == '\u{feff}');
    let author = author.map(parse_string_utf8).transpose()?;

    Ok((rest, (title, author)))
}

/// Parses a location range, and normalizes it so the positions have the
/// same most significant digit.
///
pub(crate) fn parse_location_range(s: &[u8]) -> IResult<&[u8], LocationRange, ParseErrorWithInput> {
    let (rest, (low, high)) = delimited(
        bytes::streaming::tag("Loc. "),
        alt((
            separated_pair(
                character::streaming::digit1,
                bytes::streaming::tag("-"),
                character::streaming::digit1,
            ),
            map(character::streaming::digit1, |num| (num, num)),
        )),
        bytes::complete::tag(" "),
    )(s)?;
    let low_len = low.len() as u32;
    let high_len = high.len() as u32;
    let low = parse_string_encoded_number(low)?;
    let mut high = parse_string_encoded_number(high)?;

    if low_len > high_len {
        let digit_length_diff = low_len - high_len as u32;
        let normalize_factor = 10u32.pow(low_len - digit_length_diff);
        let most_significant_digit = low / normalize_factor;

        high += normalize_factor * most_significant_digit;
    }

    Ok((
        rest,
        LocationRange {
            start: low,
            end: high,
        },
    ))
}

/// Parses created at portion of the metadata line.
///
pub(crate) fn parse_created_at(s: &[u8]) -> IResult<&[u8], DateTime<Utc>, ParseErrorWithInput> {
    // 'Added on '    indicates the start of the created at field.
    // '\n'           indicates the end of the created at field.
    let (rest, datetime) = delimited(
        bytes::streaming::tag("Added on "),
        character::streaming::not_line_ending,
        character::complete::line_ending,
    )(s)?;
    let datetime = parse_string_utf8(datetime)?;

    match NaiveDateTime::parse_from_str(datetime, "%A, %B %d, %Y, %I:%M %p") {
        Ok(ref naive_datetime) => Ok((rest, Utc.from_utc_datetime(naive_datetime))),
        Err(e) => Err(nom::Err::Failure(ParseErrorWithInput(s, e.into()))),
    }
}

/// Parses the clipping type from metadata.
///
pub(crate) fn parse_clipping_type(s: &[u8]) -> IResult<&[u8], ClippingType, ParseErrorWithInput> {
    let (rest, typ) = delimited(
        bytes::streaming::tag("- "),
        alt((
            value(ClippingType::Highlight, bytes::streaming::tag("Highlight")),
            value(ClippingType::Bookmark, bytes::streaming::tag("Bookmark")),
            value(ClippingType::Note, bytes::streaming::tag("Note")),
        )),
        opt(bytes::complete::tag(" on")),
    )(s)?;

    Ok((rest, typ))
}

/// Parses page number from metadata.
///
pub(crate) fn parse_page_number(s: &[u8]) -> IResult<&[u8], Page, ParseErrorWithInput> {
    let (rest, page) = delimited(
        bytes::streaming::tag("Page "),
        character::streaming::digit1,
        bytes::complete::tag(" "),
    )(s)?;
    let page = parse_string_encoded_number(page)?;

    Ok((rest, Page(page)))
}

/// Parses the content of a highlight or a note.
///
pub(crate) fn parse_highlight_or_note_content(
    s: &[u8],
) -> IResult<&[u8], &str, ParseErrorWithInput> {
    let (rest, highlighted) = terminated(
        character::streaming::not_line_ending,
        character::complete::line_ending,
    )(s)?;
    let highlighted = parse_string_utf8(highlighted)?;

    Ok((rest, highlighted))
}

/// Parses separator between clipping metadata.
///
fn parse_metadata_separator(s: &[u8]) -> IResult<&[u8], &[u8], ParseErrorWithInput> {
    alt((
        bytes::streaming::tag("| "),
        bytes::streaming::tag(" | "),
        bytes::streaming::tag(" "),
    ))(s)
}

/// Parses a clipping.
///
pub(crate) fn parse_clipping(s: &[u8]) -> IResult<&[u8], Clipping, ParseErrorWithInput> {
    let (rest, (title, author)) = parse_title_author_line(s)?;
    let (rest, typ) = parse_clipping_type(rest)?;
    let (rest, page) = opt(preceded(bytes::streaming::tag(" "), parse_page_number))(rest)?;

    match typ {
        ClippingType::Highlight => {
            let (rest, location) = preceded(parse_metadata_separator, parse_location_range)(rest)?;
            let (rest, created_at) = preceded(parse_metadata_separator, parse_created_at)(rest)?;
            let (rest, _) = character::streaming::line_ending(rest)?;
            let (rest, content) = parse_highlight_or_note_content(rest)?;
            let (rest, _) = parse_separator(rest)?;

            Ok((
                rest,
                Clipping::Highlight(Highlight {
                    title: title,
                    author: author,
                    page,
                    location,
                    created_at,
                    content: content,
                }),
            ))
        }
        ClippingType::Bookmark => {
            let (rest, location) =
                opt(preceded(parse_metadata_separator, parse_location_range))(rest)?;
            let (rest, created_at) = preceded(parse_metadata_separator, parse_created_at)(rest)?;
            let (rest, _) = character::streaming::line_ending(rest)?;
            let (rest, _) = character::streaming::line_ending(rest)?;
            let (rest, _) = parse_separator(rest)?;

            Ok((
                rest,
                Clipping::Bookmark(Bookmark {
                    title: title,
                    author: author,
                    page,
                    location,
                    created_at,
                }),
            ))
        }
        ClippingType::Note => {
            let (rest, location) = preceded(parse_metadata_separator, parse_location_range)(rest)?;
            let (rest, created_at) = preceded(parse_metadata_separator, parse_created_at)(rest)?;
            let (rest, _) = character::streaming::line_ending(rest)?;
            let (rest, content) = parse_highlight_or_note_content(rest)?;
            let (rest, _) = parse_separator(rest)?;

            Ok((
                rest,
                Clipping::Note(Note {
                    title: title,
                    author: author,
                    page,
                    location,
                    created_at,
                    content: content,
                }),
            ))
        }
    }
}
