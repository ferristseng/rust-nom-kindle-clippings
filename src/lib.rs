#[cfg_attr(feature = "serde_derive", macro_use)]
#[cfg(feature = "serde_derive")]
extern crate serde_derive;

use bytes::{Buf, BytesMut};
use error::{ParseError, ParseErrorWithInput, ReadParseError};
use nom::{
    combinator::{complete, iterator},
    multi::many1,
};
use std::io::Read;

mod clipping;
mod error;
mod parser;
#[cfg(test)]
mod parser_tests;

pub use clipping::{borrowed, owned, LocationRange, Page};

/// Parse Kindle clippings from a byte slice.
///
pub fn parse_clippings_bytes<'a>(s: &'a [u8]) -> Result<Vec<borrowed::Clipping<'a>>, ParseError> {
    match many1(complete(parser::parse_clipping))(s) {
        Ok((_, clippings)) => Ok(clippings),
        Err(nom::Err::Error(ParseErrorWithInput(_, e))) => return Err(e),
        Err(nom::Err::Failure(ParseErrorWithInput(_, e))) => return Err(e),
        Err(nom::Err::Incomplete(_)) => {
            // Unreachable arm because the input is the full stream of data.
            unreachable!()
        }
    }
}

/// Parse Kindle clippings from a string.
///
pub fn parse_clippings_str<'a>(s: &'a str) -> Result<Vec<borrowed::Clipping<'a>>, ParseError> {
    parse_clippings_bytes(s.as_bytes())
}

/// Parse Kindle clippings from a stream.
///
/// Note: This API is experimental. It may not be as performant as `parse_clipping_bytes`
/// or `parse_clippings_str` for inputs that can be stored in memory.
///
pub fn parse_clippings<R>(mut read: R) -> Result<Vec<owned::Clipping>, ReadParseError>
where
    R: Read,
{
    let mut clippings = Vec::new();
    let mut temp = [0; 2048];
    let mut buffer = BytesMut::with_capacity(4096);

    loop {
        let bytes_read = read.read(&mut temp)?;

        if bytes_read == 0 {
            // If there is remaining data in the buffer, try parsing it, treating it as
            // complete. `parse_clipping_bytes` will raise an error if there is any
            // remaining data in the stream because it will consider it as an incomplete
            // clipping.
            if buffer.remaining() > 0 {
                let remaining_clippings = parse_clippings_bytes(buffer.bytes())?;

                for clipping in remaining_clippings {
                    clippings.push(clipping.to_owned());
                }
            }

            break;
        }

        // FIXME: Is it possible to make this not do a copy?
        buffer.extend_from_slice(&temp[..bytes_read]);

        let mut iter = iterator(buffer.bytes(), parser::parse_clipping);

        for clipping in &mut iter {
            clippings.push(clipping.to_owned());
        }

        let remaining_bytes = match iter.finish() {
            Ok((rest, _)) => rest.len(),
            Err(nom::Err::Error(ParseErrorWithInput(_, e))) => return Err(e.into()),
            Err(nom::Err::Failure(ParseErrorWithInput(_, e))) => return Err(e.into()),
            Err(nom::Err::Incomplete(_)) => continue,
        };

        buffer.advance(buffer.remaining() - remaining_bytes);
    }

    Ok(clippings)
}
