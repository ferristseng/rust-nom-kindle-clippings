use std::{
    fmt::{self, Debug, Formatter},
    io,
    num::ParseIntError,
    str::{self, Utf8Error},
};
use thiserror::Error;

/// Error raised when the parser encounters an error.
///
#[derive(Clone, Debug, Error, PartialEq)]
pub enum ParseError {
    #[error("parse utf8 error: {0}")]
    Utf8Error(#[from] Utf8Error),

    #[error("parse datetime error: {0}")]
    ParseDateTimeError(#[from] chrono::format::ParseError),

    #[error("parse int error: {0}")]
    ParseIntError(#[from] ParseIntError),

    #[error("parse error caused by: {0:?}")]
    ParseError(nom::error::ErrorKind),
}

impl From<nom::error::ErrorKind> for ParseError {
    #[inline]
    fn from(error: nom::error::ErrorKind) -> ParseError {
        ParseError::ParseError(error)
    }
}

/// Error that includes the input that the parser failed on.
///
#[derive(Clone, PartialEq)]
pub(crate) struct ParseErrorWithInput<'a>(pub &'a [u8], pub ParseError);

impl<'a> nom::error::ParseError<&'a [u8]> for ParseErrorWithInput<'a> {
    #[inline]
    fn from_error_kind(input: &'a [u8], kind: nom::error::ErrorKind) -> Self {
        ParseErrorWithInput(input, kind.into())
    }

    #[inline]
    fn append(_: &[u8], _: nom::error::ErrorKind, other: Self) -> Self {
        other
    }
}

impl<'a> Debug for ParseErrorWithInput<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        let (input, error) = (&self.0, &self.1);

        match str::from_utf8(input) {
            Ok(s) => write!(fmt, "ParseErrorWithInput({:?}, {:?})", s, error),
            Err(_) => write!(fmt, "ParseErrorWithInput({:?}, {:?})", input, error),
        }
    }
}

/// Error raised when reading a stream or parsing.
///
#[derive(Debug, Error)]
pub enum ReadParseError {
    #[error("read error: {0}")]
    ReadError(#[from] io::Error),

    #[error("parse error: {0}")]
    ParseError(#[from] ParseError),
}
