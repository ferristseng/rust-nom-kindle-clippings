use crate::clipping::{LocationRange, Page};
use chrono::prelude::{DateTime, Utc};

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Bookmark {
    pub title: String,
    pub author: Option<String>,
    pub page: Option<Page>,
    pub location: Option<LocationRange>,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Highlight {
    pub title: String,
    pub author: Option<String>,
    pub page: Option<Page>,
    pub location: LocationRange,
    pub created_at: DateTime<Utc>,
    pub content: String,
}

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Note {
    pub title: String,
    pub author: Option<String>,
    pub page: Option<Page>,
    pub location: LocationRange,
    pub created_at: DateTime<Utc>,
    pub content: String,
}

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
#[cfg_attr(feature = "serde", serde(tag = "type"))]
pub enum Clipping {
    Bookmark(Bookmark),
    Highlight(Highlight),
    Note(Note),
}
