use crate::clipping::{owned, LocationRange, Page};
use chrono::prelude::{DateTime, Utc};

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Bookmark<'a> {
    pub title: &'a str,
    pub author: Option<&'a str>,
    pub page: Option<Page>,
    pub location: Option<LocationRange>,
    pub created_at: DateTime<Utc>,
}

impl Bookmark<'_> {
    #[inline]
    pub fn to_owned(self) -> owned::Bookmark {
        owned::Bookmark {
            title: self.title.to_string(),
            author: self.author.map(|author| author.to_string()),
            page: self.page,
            location: self.location,
            created_at: self.created_at,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Highlight<'a> {
    pub title: &'a str,
    pub author: Option<&'a str>,
    pub page: Option<Page>,
    pub location: LocationRange,
    pub created_at: DateTime<Utc>,
    pub content: &'a str,
}

impl Highlight<'_> {
    #[inline]
    pub fn to_owned(self) -> owned::Highlight {
        owned::Highlight {
            title: self.title.to_string(),
            author: self.author.map(|author| author.to_string()),
            page: self.page,
            location: self.location,
            created_at: self.created_at,
            content: self.content.to_string(),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Note<'a> {
    pub title: &'a str,
    pub author: Option<&'a str>,
    pub page: Option<Page>,
    pub location: LocationRange,
    pub created_at: DateTime<Utc>,
    pub content: &'a str,
}

impl Note<'_> {
    #[inline]
    pub fn to_owned(self) -> owned::Note {
        owned::Note {
            title: self.title.to_string(),
            author: self.author.map(|author| author.to_string()),
            page: self.page,
            location: self.location,
            created_at: self.created_at,
            content: self.content.to_string(),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize))]
#[cfg_attr(feature = "serde", serde(tag = "type"))]
pub enum Clipping<'a> {
    Bookmark(Bookmark<'a>),
    Highlight(Highlight<'a>),
    Note(Note<'a>),
}

impl Clipping<'_> {
    #[inline]
    pub fn to_owned(self) -> owned::Clipping {
        match self {
            Clipping::Bookmark(bookmark) => owned::Clipping::Bookmark(bookmark.to_owned()),
            Clipping::Highlight(highlight) => owned::Clipping::Highlight(highlight.to_owned()),
            Clipping::Note(note) => owned::Clipping::Note(note.to_owned()),
        }
    }
}
