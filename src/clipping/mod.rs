pub mod borrowed;
pub mod owned;

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Page(pub u32);

#[derive(Debug, Eq, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct LocationRange {
    pub start: u32,
    pub end: u32,
}
